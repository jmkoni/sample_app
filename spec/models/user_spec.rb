require 'spec_helper'

describe User do
  before { @user = User.new(name: "Example User", email: "user@example.com", password: "foobar", password_confirmation: "foobar") }

  subject { @user }

  describe "user name" do
    it { should respond_to(:name) }
  end

  describe "user email" do
    it { should respond_to(:email) }
  end

  describe "password digest" do
    it { should respond_to(:password_digest) }
  end

  describe "password" do
    it { should respond_to(:password) }
  end

  describe "password confirmation" do
    it { should respond_to(:password_confirmation) }
  end

  describe "remember token" do
    it { should respond_to(:remember_token) }
  end

  describe "authenticate" do
    it { should respond_to(:authenticate)}
  end

  describe "validity" do
    it { should be_valid }
  end

  context "user login" do
    context "check user name validity" do
      describe "when name is not present" do
        before { @user.name = " " }
        it { should_not be_valid }
      end

      describe "when name is too long" do
        before { @user.name = "a" * 51 }
        it { should_not be_valid }
      end
    end

    context "check email validity" do
      describe "when email is not present" do
        before { @user.email = " " }
        it { should_not be_valid }
      end

      describe "when email format is invalid" do
        it "should be invalid" do
          addresses = %w[user@foo,com user_at_foo.org bob@foo...com example.user@foo. foo@bar_baz.com foo@bar+baz.com]
          addresses.each do |invalid_address|
            @user.email = invalid_address
            expect(@user).not_to be_valid
          end
        end
      end

      describe "when email format is valid" do
        it "should be valid" do
          addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
          addresses.each do |valid_address|
            @user.email = valid_address
            expect(@user).to be_valid
          end
        end
      end

      describe "when email address is already taken" do
        before do
          user_with_same_email = @user.dup
          user_with_same_email.email = @user.email.upcase
          user_with_same_email.save
        end

        it { should_not be_valid }
      end
    end

    context "check password validity" do
      describe "when password is not present" do
        before do
          @user = User.new(name: "Example User", email: "user@example.com",
                           password: " ", password_confirmation: " ")
        end
        it { should_not be_valid }
      end

      describe "when password doesn't match confirmation" do
        before { @user.password_confirmation = "mismatch" }
        it { should_not be_valid }
      end

      describe "with a password that's too short" do
        before { @user.password = @user.password_confirmation = "a" * 5 }
        it { should be_invalid }
      end
    end
  end

  describe "return value of authenticate method" do
    before { @user.save }
    let(:found_user) { User.find_by(email: @user.email) }

    describe "with valid password" do
      it { should eq found_user.authenticate(@user.password) }
    end

    describe "with invalid password" do
      let(:user_for_invalid_password) { found_user.authenticate("invalid") }

      it { should_not eq user_for_invalid_password }
      specify { expect(user_for_invalid_password).to be_false }
    end

    describe "remember token" do
      before { @user.save }
      its(:remember_token) { should_not be_blank }
    end
  end
end
