require 'spec_helper'

describe "Authentication" do

  subject { page }

  describe "signin" do
    before { visit signin_path }

    describe "with invalid information" do
      before { click_button "Sign in" }

      describe "should have correct title" do
        it { should have_title('Sign in') }
      end

      describe "should have alert-error selector" do
        it { should have_selector('div.alert.alert-error') }
      end

      describe "after visiting another page" do
        before { click_link "Home" }
        it { should_not have_selector('div.alert.alert-error') }
      end
    end

    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        fill_in "Email",    with: user.email.upcase
        fill_in "Password", with: user.password
        click_button "Sign in"
      end

      describe "should have user name as title" do
        it { should have_title(user.name) }
      end

      describe "should have link to profile" do
        it { should have_link('Profile',     href: user_path(user)) }
      end

      describe "should have link to sign out" do
        it { should have_link('Sign out',    href: signout_path) }
      end

      describe "should not have link to sign in" do
        it { should_not have_link('Sign in', href: signin_path) }
      end

      describe "followed by signout" do
        before { click_link "Sign out" }
        it { should have_link('Sign in') }
      end
    end
  end
end
