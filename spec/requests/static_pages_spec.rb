require 'spec_helper'
require 'support/utilities'

describe "Static pages" do

  subject { page }

  shared_examples_for "all static pages" do
    describe "header selector" do
      it { should have_selector('h1', text: heading) }
    end

    describe "should have page title" do
      it { should have_title(full_title(page_title)) }
    end
  end

  describe "Home page" do
    before { visit root_path }
    let(:heading)    { 'Sample App' }
    let(:page_title) { '' }

    describe "should behave like a staic page" do
      it_should_behave_like "all static pages"
    end
    describe "should have home in the title" do
      it { should_not have_title('| Home') }
    end
  end

  describe "Help page" do
    before { visit help_path }
    let(:heading)   {'Help'}
    let(:page_title) {'Help'}

    describe "should behave like a staic page" do
      it_should_behave_like "all static pages"
    end
    describe "should have 'help' in the content" do
      it { should have_content('Help') }
    end
  end

  describe "About page" do
    before { visit about_path }
    let(:heading)   {'About'}
    let(:page_title) {'About'}

    describe "should behave like a staic page" do
      it_should_behave_like "all static pages"
    end
    describe "should have 'about us' in the content" do
      it { should have_content('About Us') }
    end
  end

  describe "Contact page" do
    before { visit contact_path }
    let(:heading)   {'Contact'}
    let(:page_title) {'Contact'}

    describe "should behave like a staic page" do
      it_should_behave_like "all static pages"
    end
    describe "should have header called 'Contact'" do
      it { should have_selector('h1', text: 'Contact') }
    end
  end

  it "should have the right links on the layout" do
    visit root_path
    click_link "About"
    expect(page).to have_title(full_title('About'))
    click_link "Help"
    expect(page).to have_title(full_title('Help'))
    click_link "Contact"
    expect(page).to have_title(full_title('Contact'))
    click_link "Home"
    click_link "Sign up now!"
    expect(page).to have_title(full_title('Sign Up'))
    click_link "sample app"
    # expect(page).to # fill in
  end
end
