FactoryGirl.define do
  factory :user do
    name     "Jennifer Konikowski"
    email    "jennifer@example.com"
    password "foobar"
    password_confirmation "foobar"
  end
end